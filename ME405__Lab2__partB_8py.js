var ME405__Lab2__partB_8py =
[
    [ "ch1_isr", "ME405__Lab2__partB_8py.html#acb03815c35b2954f868dc7a3c8ba8221", null ],
    [ "ch2_isr", "ME405__Lab2__partB_8py.html#a4c4a4b10035ad591173a651e99408dc2", null ],
    [ "average", "ME405__Lab2__partB_8py.html#a9c7e683fb92f5230a54ff30074cfb0cf", null ],
    [ "ch1", "ME405__Lab2__partB_8py.html#a21095fc3329635423998348787484a7e", null ],
    [ "ch2", "ME405__Lab2__partB_8py.html#adfc506eeb4e5fec1bbc5109f9604b79d", null ],
    [ "cnt", "ME405__Lab2__partB_8py.html#a0dcd95dca8c59d851837610882a4598c", null ],
    [ "game_on", "ME405__Lab2__partB_8py.html#a7aa5b593991f9a73a155d130c58d2a21", null ],
    [ "LED1", "ME405__Lab2__partB_8py.html#a06d515087acdf1cd53b73b89a603be7e", null ],
    [ "limit", "ME405__Lab2__partB_8py.html#ae0a789e572ecbe2017dc9491ef6c79b1", null ],
    [ "period", "ME405__Lab2__partB_8py.html#ac6b9f15032a2ebdeb2dfd50364a2365d", null ],
    [ "prescaler", "ME405__Lab2__partB_8py.html#a5fb73c63e1cbcc4a928719272764b002", null ],
    [ "scores", "ME405__Lab2__partB_8py.html#adda1614958510af83c984d144e94e8f9", null ],
    [ "start", "ME405__Lab2__partB_8py.html#a2b4ec8623b46c446eeb6fa7fecb0dc16", null ],
    [ "time_start", "ME405__Lab2__partB_8py.html#a35c1f4612732e5f269d565ac3d1491d9", null ],
    [ "timer2", "ME405__Lab2__partB_8py.html#a81212db6f213140c3c8968ce69ea8487", null ]
];