var namespaces_dup =
[
    [ "Controller", null, [
      [ "Ctrl", "classController_1_1Ctrl.html", "classController_1_1Ctrl" ]
    ] ],
    [ "cotask", null, [
      [ "Task", "classcotask_1_1Task.html", "classcotask_1_1Task" ],
      [ "TaskList", "classcotask_1_1TaskList.html", "classcotask_1_1TaskList" ],
      [ "task_list", "cotask_8py.html#ae54e25f8d14958f642bcc22ddeb6c56f", null ]
    ] ],
    [ "Encoder", null, [
      [ "EncoderDriver", "classEncoder_1_1EncoderDriver.html", "classEncoder_1_1EncoderDriver" ],
      [ "enc", "Encoder_8py.html#a5a8c47f1006776b7364917744ec87498", null ]
    ] ],
    [ "FindChange", null, [
      [ "getChange", "FindChange_8py.html#a57079cd0e5e19e3e44b83492ea1abee5", null ],
      [ "main", "FindChange_8py.html#a6ef370d6b1d6b84c93e3ee422623aac7", null ]
    ] ],
    [ "Lab0x01", "namespaceLab0x01.html", null ],
    [ "main", null, [
      [ "ctrl_task", "TermProject_2main_8py.html#a1d9e05dbc69d1e40e192647aa2178521", null ],
      [ "data_task", "TermProject_2main_8py.html#a8a74c41fb93f5502bbb54222cc590304", null ],
      [ "encode_task", "TermProject_2main_8py.html#ad05721f31d28395d9bf3c3456333c214", null ],
      [ "motor_task", "TermProject_2main_8py.html#a22c02d46e13fef03586e7261503784e6", null ],
      [ "touch_task", "TermProject_2main_8py.html#a9dcb9d48e3ee22c4ff67ef482e1bfe04", null ],
      [ "UI_task", "TermProject_2main_8py.html#a4c99f2c8eaadf389b66063ab1acdc5d3", null ],
      [ "adcALL", "Lab4_2main_8py.html#add84348e720d2f2dae39bd206af39727", null ],
      [ "cel", "Lab4_2main_8py.html#abcc241060991b2359e9a7b353cc0d929", null ],
      [ "debug_count", "TermProject_2main_8py.html#a3d18e3b48a5df7effd4acee5c47b454d", null ],
      [ "queue_ang", "TermProject_2main_8py.html#a83dc4dbeb630401fb69d55423570c234", null ],
      [ "queue_end", "TermProject_2main_8py.html#a1c2989f51ccf99f8ac5c6bbae29a2d48", null ],
      [ "queue_motor1", "TermProject_2main_8py.html#adf358b481d64839594cddd7e16cd80fc", null ],
      [ "queue_motor2", "TermProject_2main_8py.html#a51ebc4e2267a644b5cf939ec5e533de9", null ],
      [ "queue_vang", "TermProject_2main_8py.html#acf4de1ea89e1fe020a55265e1d3f7bb1", null ],
      [ "queue_x", "TermProject_2main_8py.html#a099083add37190ff0e80dc4ccb578009", null ],
      [ "queue_y", "TermProject_2main_8py.html#ae8a91ddbf2d97b154e4d74b3ddcb4caa", null ],
      [ "sensor", "Lab4_2main_8py.html#a86cc0c2d3b40e8d33d8dd98b459621a2", null ],
      [ "task_ctrl", "TermProject_2main_8py.html#ae1c78a5a9024ab1aff1291029b2f7c80", null ],
      [ "task_enc", "TermProject_2main_8py.html#a63b233af19a20c20b766224b90c4819f", null ],
      [ "task_mot", "TermProject_2main_8py.html#a228b43336567159754805fe96b407cb3", null ],
      [ "task_tp", "TermProject_2main_8py.html#ae5b0ca497789c4d8d580c6462e2db703", null ],
      [ "temp", "Lab4_2main_8py.html#a72d4940f3cafefdc5c0619413cb02b4b", null ],
      [ "theta_x_dot_vals", "TermProject_2main_8py.html#af89be64e259e709d7b89fd36ee26becd", null ],
      [ "theta_x_vals", "TermProject_2main_8py.html#a1d9ff33e8d72f1b81d9d298e337c21ed", null ],
      [ "theta_y_dot_vals", "TermProject_2main_8py.html#a14d1f34f3b163361458adac402b3b1a4", null ],
      [ "theta_y_vals", "TermProject_2main_8py.html#a7e00ef6c7a3cddcf3767f313e152de15", null ],
      [ "time", "Lab4_2main_8py.html#a7047ceda29820f67f4b6c0d012764681", null ],
      [ "time_init", "Lab4_2main_8py.html#a12810804e1717e6e7abd7a4eaefeab3e", null ],
      [ "v", "Lab4_2main_8py.html#a93eaef921d00e8cf53c272146a9d8563", null ],
      [ "x_dot_vals", "TermProject_2main_8py.html#a7bf2e29f8ffcad0fa665dfcd54383396", null ],
      [ "x_vals", "TermProject_2main_8py.html#add7b621500f261e27b002da11e1631a9", null ],
      [ "y_dot_vals", "TermProject_2main_8py.html#a80a23d55466cf24973d616b01f0706e4", null ],
      [ "y_vals", "TermProject_2main_8py.html#aa83c2d887e85d1ab70800ee2cbf8be7a", null ]
    ] ],
    [ "mcp9808", null, [
      [ "TempSense", "classmcp9808_1_1TempSense.html", "classmcp9808_1_1TempSense" ],
      [ "reader", "mcp9808_8py.html#aab758ee33d82317bae6335ecca1d3eb0", null ]
    ] ],
    [ "ME405_Lab0x01", null, [
      [ "kb_cb", "ME405__Lab0x01_8py.html#a53631a2886fc85ae690f63d2ebc10a4d", null ],
      [ "VendotronTask", "ME405__Lab0x01_8py.html#ad5ca37a74f2b87c78a06756fa16d07e8", null ],
      [ "callback", "ME405__Lab0x01_8py.html#a780d003e7525d0d89779d1ba332d8221", null ],
      [ "last_key", "ME405__Lab0x01_8py.html#a0aaf28919416c9d98211d5fa5fea5632", null ],
      [ "vender", "ME405__Lab0x01_8py.html#ac2d4490ca46acdcead6eb534b363f357", null ]
    ] ],
    [ "ME405_Lab2_partA", null, [
      [ "fun", "ME405__Lab2__partA_8py.html#acfb1f7bd0d0255d9f7814d86c410f832", null ],
      [ "average", "ME405__Lab2__partA_8py.html#acf2d12848781001987d51fd0a3744ce2", null ],
      [ "button_pressed", "ME405__Lab2__partA_8py.html#a97d7196b2900894e0393037004fb5546", null ],
      [ "cnt", "ME405__Lab2__partA_8py.html#a97c0927a8594d353ca6dc571c20b370c", null ],
      [ "game_on", "ME405__Lab2__partA_8py.html#a944e4d3bc5376557cdb9fd9aba9a8419", null ],
      [ "LED1", "ME405__Lab2__partA_8py.html#a3aabb639960d139f3dfeb0a4c3f0f58d", null ],
      [ "scores", "ME405__Lab2__partA_8py.html#a978ea8c89ca823ed87953808909c5558", null ],
      [ "SwitchA", "ME405__Lab2__partA_8py.html#ab995fc048728a8ba5a8ae9383a18d81b", null ],
      [ "time_diff", "ME405__Lab2__partA_8py.html#a84fb5c82153e0a523743f7b23f11f47d", null ],
      [ "time_start", "ME405__Lab2__partA_8py.html#a32483c4087e754e434b706a286d0c17e", null ]
    ] ],
    [ "ME405_Lab2_partB", null, [
      [ "ch1_isr", "ME405__Lab2__partB_8py.html#acb03815c35b2954f868dc7a3c8ba8221", null ],
      [ "ch2_isr", "ME405__Lab2__partB_8py.html#a4c4a4b10035ad591173a651e99408dc2", null ],
      [ "average", "ME405__Lab2__partB_8py.html#a9c7e683fb92f5230a54ff30074cfb0cf", null ],
      [ "ch1", "ME405__Lab2__partB_8py.html#a21095fc3329635423998348787484a7e", null ],
      [ "ch2", "ME405__Lab2__partB_8py.html#adfc506eeb4e5fec1bbc5109f9604b79d", null ],
      [ "cnt", "ME405__Lab2__partB_8py.html#a0dcd95dca8c59d851837610882a4598c", null ],
      [ "game_on", "ME405__Lab2__partB_8py.html#a7aa5b593991f9a73a155d130c58d2a21", null ],
      [ "LED1", "ME405__Lab2__partB_8py.html#a06d515087acdf1cd53b73b89a603be7e", null ],
      [ "limit", "ME405__Lab2__partB_8py.html#ae0a789e572ecbe2017dc9491ef6c79b1", null ],
      [ "period", "ME405__Lab2__partB_8py.html#ac6b9f15032a2ebdeb2dfd50364a2365d", null ],
      [ "prescaler", "ME405__Lab2__partB_8py.html#a5fb73c63e1cbcc4a928719272764b002", null ],
      [ "scores", "ME405__Lab2__partB_8py.html#adda1614958510af83c984d144e94e8f9", null ],
      [ "start", "ME405__Lab2__partB_8py.html#a2b4ec8623b46c446eeb6fa7fecb0dc16", null ],
      [ "time_start", "ME405__Lab2__partB_8py.html#a35c1f4612732e5f269d565ac3d1491d9", null ],
      [ "timer2", "ME405__Lab2__partB_8py.html#a81212db6f213140c3c8968ce69ea8487", null ]
    ] ],
    [ "ME405_Lab2_partB - Copy", null, [
      [ "ch1_isr", "ME405__Lab2__partB_01-_01Copy_8py.html#a935a2e682d764e38650789d19c95bc82", null ],
      [ "ch2_isr", "ME405__Lab2__partB_01-_01Copy_8py.html#a1fbcf071975b15c44fb54d8ecd26527f", null ],
      [ "average", "ME405__Lab2__partB_01-_01Copy_8py.html#a6a0ba4ced5860c936799091a5586ea2e", null ],
      [ "ch1", "ME405__Lab2__partB_01-_01Copy_8py.html#a681c625afcdb90d20bd406a7e5604cb2", null ],
      [ "ch2", "ME405__Lab2__partB_01-_01Copy_8py.html#a7c5828ea7dedf9375d442dde12bae86d", null ],
      [ "cnt", "ME405__Lab2__partB_01-_01Copy_8py.html#acf98c59d4387ca252043b0c51e53cd5c", null ],
      [ "game_on", "ME405__Lab2__partB_01-_01Copy_8py.html#a11fe6906a8a919d06e300426bd5f577a", null ],
      [ "LED1", "ME405__Lab2__partB_01-_01Copy_8py.html#a815d4a58cc468eb475c515133ec297f0", null ],
      [ "limit", "ME405__Lab2__partB_01-_01Copy_8py.html#ac76d014ea26bc08e97b3cfc0978ac655", null ],
      [ "period", "ME405__Lab2__partB_01-_01Copy_8py.html#a2f29cf2a392c5dc668110a9bfb7fa661", null ],
      [ "prescaler", "ME405__Lab2__partB_01-_01Copy_8py.html#afa8dc06217973050b706e285019884a9", null ],
      [ "scores", "ME405__Lab2__partB_01-_01Copy_8py.html#a88d2872cc8ddcd93630f11d0c955cefc", null ],
      [ "start", "ME405__Lab2__partB_01-_01Copy_8py.html#a48d54f799b7388f0ad9338abd86a73ad", null ],
      [ "time_start", "ME405__Lab2__partB_01-_01Copy_8py.html#a8735159e92ac356aaa22e3d5a68d681a", null ],
      [ "timer2", "ME405__Lab2__partB_01-_01Copy_8py.html#a6ab4e5730a3a425651f9ae1f25f623fd", null ]
    ] ],
    [ "ME405_Lab3", null, [
      [ "adc", "ME405__Lab3_8py.html#adc5bc35303f5b8871153ac2119a4bdfa", null ],
      [ "buffy", "ME405__Lab3_8py.html#a15636c11416d8499ff5f610b8d69fc3c", null ],
      [ "LED1", "ME405__Lab3_8py.html#a762a98f24a66fbf24db6ca686b945f7a", null ],
      [ "pin", "ME405__Lab3_8py.html#aa3711d1d0e415632f1ab07c6ae162917", null ],
      [ "SwitchA", "ME405__Lab3_8py.html#a43d709e8f01370c9347eeed9d99d80c3", null ],
      [ "uart", "ME405__Lab3_8py.html#aca53da6f2a1cdccc120941a4ccae76f8", null ],
      [ "val", "ME405__Lab3_8py.html#abee313f98608ba7f01669e613a21e67f", null ]
    ] ],
    [ "ME405_Lab3pc", null, [
      [ "ch", "ME405__Lab3pc_8py.html#a792a60020dceb53edb1fc4504c822d19", null ],
      [ "cnt2", "ME405__Lab3pc_8py.html#a775f407e742f827dfbe7aa4233a559e0", null ],
      [ "data", "ME405__Lab3pc_8py.html#a0304dce0c7f293e8c11f2405a1743097", null ],
      [ "data_trimmed", "ME405__Lab3pc_8py.html#ae51b44271deb4ae8567b8a588b03c81f", null ],
      [ "encoding", "ME405__Lab3pc_8py.html#a12a88feb0e6fe8221ca540103e208e11", null ],
      [ "line", "ME405__Lab3pc_8py.html#ac8bf1d619c61e7f6ede167897c15cddb", null ],
      [ "loc1", "ME405__Lab3pc_8py.html#a40c8ea63af5bfe3ae9a2eccf061e5adf", null ],
      [ "loc2", "ME405__Lab3pc_8py.html#a999fe8396dbb9777ff5aa9f07379dd52", null ],
      [ "newline", "ME405__Lab3pc_8py.html#ab6122e08812106789fd0daba9ca69377", null ],
      [ "ser", "ME405__Lab3pc_8py.html#a58a369519b01c05139f873ed397ccc3d", null ],
      [ "time_trimmed", "ME405__Lab3pc_8py.html#a5bc452c90d4ff5a616e25faa4b2e5b4c", null ],
      [ "w", "ME405__Lab3pc_8py.html#a9d7a6cd4605f17e9a561ef8734e23056", null ]
    ] ],
    [ "Motor", null, [
      [ "MotorDriver", "classMotor_1_1MotorDriver.html", "classMotor_1_1MotorDriver" ]
    ] ],
    [ "plot", null, [
      [ "label", "plot_8py.html#ab833ccc4e4fb7e634ddb35e4eb25c59d", null ],
      [ "line", "plot_8py.html#a72f95127a17220dcf0adf38386b72ae4", null ],
      [ "temp_amb", "plot_8py.html#ab8f3a1dd07c170981028a74bfc5f92b7", null ],
      [ "temp_int", "plot_8py.html#a4a460e8e06f1f11e6ec68742abd36319", null ],
      [ "time", "plot_8py.html#a7de8b5000b215d5a9951ed6b07fcd065", null ]
    ] ],
    [ "task_share", null, [
      [ "Queue", "classtask__share_1_1Queue.html", "classtask__share_1_1Queue" ],
      [ "Share", "classtask__share_1_1Share.html", "classtask__share_1_1Share" ],
      [ "show_all", "task__share_8py.html#a130cad0bc96d3138e77344ea85586b7c", null ],
      [ "share_list", "task__share_8py.html#a75818e5b662453e3723d0f234c85e519", null ]
    ] ],
    [ "TermProject", "namespaceTermProject.html", null ],
    [ "TouchPanel", null, [
      [ "TouchDriver", "classTouchPanel_1_1TouchDriver.html", "classTouchPanel_1_1TouchDriver" ],
      [ "diff", "TouchPanel_8py.html#adc2b9dc05727a973d2be03594320766e", null ],
      [ "end", "TouchPanel_8py.html#a1231da2fccb86c38b5b76990b209f858", null ],
      [ "list", "TouchPanel_8py.html#aaa30f57f5e21b3bc8b20791f5063ceeb", null ],
      [ "panel", "TouchPanel_8py.html#aecd2196ae412b001355c405dff92b2fc", null ],
      [ "start", "TouchPanel_8py.html#a5a6fd16e982e50f60ee93c26c33d5467", null ]
    ] ]
];