/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "ME405 Portfolio: Craig Belshe", "index.html", [
    [ "Portfolio Details", "index.html#sec_port", null ],
    [ "ME405: Term Project", "index.html#FinalProject", null ],
    [ "Lab 0x01 Documentation", "index.html#sec_lab1", null ],
    [ "Lab 0x02 Documentation", "index.html#sec_lab2", null ],
    [ "Lab 0x03 Documentation", "index.html#sec_lab3", null ],
    [ "Lab 0x04 Documentation", "index.html#sec_lab4", null ],
    [ "HW 0x02 Results", "index.html#sec_hw2", null ],
    [ "HW 0x04 Results", "index.html#sec_hw4", null ],
    [ "HW 0x05 Results", "index.html#sec_hw5", null ],
    [ "Homework 2", "HW2.html", null ],
    [ "Homework 4", "HW4.html", null ],
    [ "Homework 5", "HW5.html", [
      [ "Calculations", "HW5.html#section_1", null ],
      [ "Simulation", "HW5.html#section_2", null ]
    ] ],
    [ "Lab 3 Data", "Lab3data.html", [
      [ "Program Outputs", "Lab3data.html#sectionP", [
        [ "Raw Data", "Lab3data.html#data", null ],
        [ "Data Plot", "Lab3data.html#plot", null ]
      ] ],
      [ "Time Constant", "Lab3data.html#section2", [
        [ "Theoretical Calculated Value for Time Constant", "Lab3data.html#theory", null ],
        [ "Time Constant from Measured Data", "Lab3data.html#datat", null ]
      ] ]
    ] ],
    [ "Lab 4 Data", "Lab4data.html", null ],
    [ "TouchPanel Details", "Touch_Panel_Details.html", [
      [ "Calibration of Touch Panel", "Touch_Panel_Details.html#Cal", null ],
      [ "Benchmarks for Timing", "Touch_Panel_Details.html#Ben", [
        [ "Scan_Z Test:", "Touch_Panel_Details.html#scanz", null ],
        [ "Scan_Y Test:", "Touch_Panel_Details.html#scany", null ],
        [ "Scan_X Test:", "Touch_Panel_Details.html#scanx", null ],
        [ "Scan_ALL Test:", "Touch_Panel_Details.html#scanall", null ],
        [ "Summary of Benchmarks", "Touch_Panel_Details.html#scansum", null ]
      ] ],
      [ "Usage of Touch Panel Driver", "Touch_Panel_Details.html#us", null ]
    ] ],
    [ "Motor Details", "Motor_Details.html", [
      [ "Video Demonstration", "Motor_Details.html#mdemo", null ],
      [ "Dutycycle Benchmark", "Motor_Details.html#bdc", null ]
    ] ],
    [ "Encoder Details", "Encoder_Details.html", null ],
    [ "Controller Details", "Ctrl_Details.html", null ],
    [ "Term Project", "TermProject_details.html", [
      [ "Scheduler", "TermProject_details.html#scheduler", null ],
      [ "Video Demonstration", "TermProject_details.html#videodemo", null ],
      [ "Platform Data", "TermProject_details.html#dta", null ],
      [ "Major Issues", "TermProject_details.html#err", [
        [ "Touch Panel Issues", "TermProject_details.html#tp_iss", null ],
        [ "Motor Issues", "TermProject_details.html#mot_iss", null ],
        [ "Encoder Issues", "TermProject_details.html#enc_iss", null ],
        [ "Scheduler Issues", "TermProject_details.html#sche_iss", null ],
        [ "Board Issues", "TermProject_details.html#brd_iss", null ]
      ] ]
    ] ],
    [ "Packages", "namespaces.html", [
      [ "Packages", "namespaces.html", "namespaces_dup" ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"",
"TouchPanel_8py.html#aecd2196ae412b001355c405dff92b2fc"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';