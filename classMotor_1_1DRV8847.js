var classMotor_1_1DRV8847 =
[
    [ "__init__", "classMotor_1_1DRV8847.html#ab09b71122d6352353abb3bba181cf7bf", null ],
    [ "clear_fault", "classMotor_1_1DRV8847.html#a7d0da321eee332056c3c3da3a0d9707c", null ],
    [ "disable", "classMotor_1_1DRV8847.html#a82831cd3251e4a6cc428c5fa8c62728e", null ],
    [ "enable", "classMotor_1_1DRV8847.html#ac86935c31b301cc162b6ea3a29a33f5b", null ],
    [ "fault_CB", "classMotor_1_1DRV8847.html#ae4ce316d84a846652ba3edad5f16ce8a", null ],
    [ "set_level_1", "classMotor_1_1DRV8847.html#ab4ee80db0278178083da1ead6772b6c1", null ],
    [ "set_level_2", "classMotor_1_1DRV8847.html#a83894e9c08c515ac6dd2694a4477808f", null ],
    [ "ch1", "classMotor_1_1DRV8847.html#ac9f22fe740956bdce4d92e3ec305ea50", null ],
    [ "ch2", "classMotor_1_1DRV8847.html#a049d8ddb259dd89e5667a9407eee41e8", null ],
    [ "ch3", "classMotor_1_1DRV8847.html#aedcf4e283b774ba0d8d2c75cee64c704", null ],
    [ "ch4", "classMotor_1_1DRV8847.html#a8e71e387b01a1887160cf07be1ca15ee", null ],
    [ "enabled", "classMotor_1_1DRV8847.html#a37cc3545da0a3c140de8e8c1540ea021", null ],
    [ "freq", "classMotor_1_1DRV8847.html#a37f974cd0df484e45c85a382fe12f7c7", null ],
    [ "m1m", "classMotor_1_1DRV8847.html#a0725a87f85ce47f54626b7cfd2f40eef", null ],
    [ "m1p", "classMotor_1_1DRV8847.html#add2995fce29fb9cbb8b48e88d1328a32", null ],
    [ "m2m", "classMotor_1_1DRV8847.html#adbbb04f13fb31af5c559ed5c4faa91c9", null ],
    [ "m2p", "classMotor_1_1DRV8847.html#a7989db277f75686e3c7af2a571d5423a", null ],
    [ "nfault", "classMotor_1_1DRV8847.html#aabd8aef753d150ca0c9ca068dd23d06a", null ],
    [ "nsleep", "classMotor_1_1DRV8847.html#aa3cef457df7674d84a0b1454cf99c906", null ],
    [ "tim", "classMotor_1_1DRV8847.html#aa353eab0e05a1ad14d81fe6b46191b06", null ]
];