var annotated_dup =
[
    [ "Controller", null, [
      [ "Ctrl", "classController_1_1Ctrl.html", "classController_1_1Ctrl" ]
    ] ],
    [ "cotask", null, [
      [ "Task", "classcotask_1_1Task.html", "classcotask_1_1Task" ],
      [ "TaskList", "classcotask_1_1TaskList.html", "classcotask_1_1TaskList" ]
    ] ],
    [ "Encoder", null, [
      [ "EncoderDriver", "classEncoder_1_1EncoderDriver.html", "classEncoder_1_1EncoderDriver" ]
    ] ],
    [ "mcp9808", null, [
      [ "TempSense", "classmcp9808_1_1TempSense.html", "classmcp9808_1_1TempSense" ]
    ] ],
    [ "Motor", null, [
      [ "MotorDriver", "classMotor_1_1MotorDriver.html", "classMotor_1_1MotorDriver" ]
    ] ],
    [ "task_share", null, [
      [ "Queue", "classtask__share_1_1Queue.html", "classtask__share_1_1Queue" ],
      [ "Share", "classtask__share_1_1Share.html", "classtask__share_1_1Share" ]
    ] ],
    [ "TouchPanel", null, [
      [ "TouchDriver", "classTouchPanel_1_1TouchDriver.html", "classTouchPanel_1_1TouchDriver" ]
    ] ]
];